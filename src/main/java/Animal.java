public abstract class Animal {

    boolean isSleeping;
    boolean isEating;


    public void sleep() {
        isSleeping = true;
    }

    public void eat() {
        isSleeping = false;
        isEating = true;
    }

    public abstract String voice();

    public void out(){
        System.out.println(voice());
    }


}
