public class MyAnimals {
    public static void main(String[] args) {
        Cat myCat = new Cat();
        myCat.sleep();
        myCat.eat();
        myCat.voice();
        myCat.out();

        Cow myCow = new Cow();
        myCow.sleep();
        myCow.eat();
        myCow.voice();
        myCow.out();

        Rabbit myRabbit = new Rabbit();
        myRabbit.voice();
        myRabbit.out();
        myRabbit.eat();
        myRabbit.sleep();
    }
}
